interface Category {
  __typename?: string;
  alias: string;
  title: string;
}

export default Category;
