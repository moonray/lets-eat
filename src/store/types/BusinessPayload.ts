import Business from './Business';

interface BusinessPayload {
  __typename?: string;
  businesses: Business[];
  total: number;
  region: {
    __typename?: string;
    longitude: number;
    latitude: number;
  };
}

export default BusinessPayload;
