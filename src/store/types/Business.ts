import Category from './Category';
import Location from './Location';

interface Business {
  __typename?: string;
  id: string;
  alias: string;
  image_url: string;
  is_closed: boolean;
  name: string;
  url: string;
  review_count: number;
  categories: Category[];
  rating: number;
  coordinates: {
    __typename?: string;
    longitude: string;
    latitude: string;
  };
  price: string;
  location: Location;
  phone: string;
  display_phone: string;
  distance: number;
}

export default Business;
