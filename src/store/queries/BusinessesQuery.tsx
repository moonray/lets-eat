import { loader } from 'graphql.macro';
import { Query } from 'react-apollo';

import BusinessPayload from '../types/BusinessPayload';

const GQL_GET_BUSINESSES = loader('../graphql/Businesses.graphql');
export { GQL_GET_BUSINESSES };

export interface BusinessesVariables {
    location: string;
    limit: number;
}

export interface BusinessValues {
    payload: BusinessPayload;
}

class BusinessesQuery extends Query<BusinessValues, BusinessesVariables> {}

export default BusinessesQuery;
