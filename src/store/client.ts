import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloClient } from 'apollo-client';
import { RestLink } from 'apollo-link-rest';

const restLink = new RestLink({
  uri: 'https://cyxf2dye2m.execute-api.us-east-1.amazonaws.com/default/',
});

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: restLink,
});

export default client;
