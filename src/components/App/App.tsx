import React, { Component } from 'react';
import { ApolloProvider } from 'react-apollo';

import client from '../../store/client';
import BusinessList from '../BusinessList';
import Search from '../Search';

import styles from './App.module.css';

interface AppState {
  location: string;
}

class App extends Component<{}, AppState> {
  public state: AppState = {
    location: '',
  };

  public render() {
    const { location } = this.state;

    return (
      <ApolloProvider client={client}>
        <div className={styles.root}>
          <header className={styles.header}>
            <Search onChange={this.handleChange} value={location} />
          </header>
          <section className={styles.results}>
            <BusinessList limit={1} location={location} />
          </section>
          <footer className={styles.footer}>Copyright © 2019 Nobody in particular, Inc.</footer>
        </div>
      </ApolloProvider>
    );
  }

  private handleChange = (value: string) => {
    this.setState({ location: value });
  };
}

export default App;
