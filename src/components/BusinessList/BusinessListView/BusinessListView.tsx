import { ApolloError } from 'apollo-client';
import classNames from 'classnames';
import React from 'react';

import Business from '../../../store/types/Business';
import BusinessItem from './BusinessItem';

import styles from './BusinessListView.module.css';

export interface BusinessListViewProps {
  businesses: Business[];
  loading: boolean;
  error?: ApolloError;
}

const BusinessListView: React.SFC<BusinessListViewProps> = ({
  businesses = [],
  loading,
  error,
}) => {
  const loadingEl = <div className={styles.loading}>Loading...</div>;
  const errorEl = (
    <div className={styles.error}>
      An error occurred while trying to load a list of restaurants.
    </div>
  );

  return (
    <div className={classNames(styles.root, {[styles.empty]: !loading && !error && !businesses.length})}>
      {loading
        ? loadingEl
        : error
        ? errorEl
        : businesses.map(business => <BusinessItem business={business} key={business.id} />)}
    </div>
  );
};

export default BusinessListView;
