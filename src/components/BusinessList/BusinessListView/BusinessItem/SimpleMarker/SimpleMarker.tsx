import { Coords } from 'google-map-react';
import React from 'react';

import styles from './SimpleMarker.module.css';

export interface SimpleMarkerProps extends Coords {
  text: string;
  url: string;
}

export const SimpleMarker: React.SFC<SimpleMarkerProps> = ({ text, url }) => (
  <div className={styles.root}><a href={url}>{text}</a></div>
);

export default SimpleMarker;
