import classNames from 'classnames';
import GoogleMapReact, { Coords } from 'google-map-react';
import React, { Fragment } from 'react';

import Business from '../../../../store/types/Business';

import styles from './BusinessItem.module.css';
import SimpleMarker from './SimpleMarker/SimpleMarker';

export interface BusinessListViewProps {
  business: Business;
}

const BusinessItem: React.SFC<BusinessListViewProps> = ({ business }) => (
  <div className={styles.root}>
    <div>
      <a href={business.url}>
        <span className={styles.imageWrapper}>
          <img src={business.image_url} className={styles.image} />
        </span>
      </a>
    </div>
    <div>
      <div className={styles.name}>
        <a href={business.url}>{business.name}</a>
        <span
          className={classNames(
            styles.detail,
            styles.fav,
            styles[`fav${String(business.rating).replace('.', '')}`]
          )}
        />
        <span className={styles.detail}>{business.price}</span>
      </div>
      <div className={styles.details}>
        <div className={classNames(styles.detail, styles.categories)}>
          {business.categories.map(category => (<span>{category.title}</span>))}
        </div>
        <div className={styles.detail}><a href={`tel:${business.display_phone}`}>{business.display_phone}</a></div>
        <div className={styles.detail}>
          {business.location.display_address.map(line => (
            <Fragment>
              {line}
              <br />
            </Fragment>
          ))}
        </div>
        {business.is_closed ? (
          <div className={classNames(styles.detail, styles.closed)}>
            This location is permanently closed!
          </div>
        ) : null}
      </div>
    </div>
    <div className={styles.map}>
      <GoogleMapReact
        // bootstrapURLKeys={{ key: /* YOUR KEY HERE */ }}
        defaultCenter={
          {
            lat: +business.coordinates.latitude,
            lng: +business.coordinates.longitude,
          } as Coords
        }
        defaultZoom={11}
      >
        <SimpleMarker
          lat={+business.coordinates.latitude}
          lng={+business.coordinates.longitude}
          text={business.name}
          url={business.url}
        />
      </GoogleMapReact>
    </div>
  </div>
);

export default BusinessItem;
