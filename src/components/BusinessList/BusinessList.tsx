import React from 'react';

import BusinessQuery, {
  BusinessValues,
  GQL_GET_BUSINESSES,
} from '../../store/queries/BusinessesQuery';
import Business from '../../store/types/Business';
import BusinessListView from './BusinessListView';

export interface BusinessListProps {
  limit?: number;
  location: string;
}

const BusinessList: React.StatelessComponent<BusinessListProps> = ({ limit = 5, location }) => (
  <BusinessQuery query={GQL_GET_BUSINESSES} variables={{ limit: 20, location }} skip={!location}>
    {({
      data: { payload = {} as BusinessValues['payload'] } = {} as BusinessValues,
      loading,
      error,
    }) => {
      // Can't simply destructure because value can be null.
      const businesses: Business[] = payload.businesses || [];

      window.console.log('BusinessList', businesses);

      return <BusinessListView businesses={filterRandom(businesses, 1)} loading={loading} error={error} />;
    }}
  </BusinessQuery>
);

const filterRandom = (arr: any[], n: number) => {
  const result = new Array(n);
  let len = arr.length;
  const taken = new Array(len);

  if (n > len) {
    return arr;
  }
  while (n--) {
    const x = Math.floor(Math.random() * len);
    result[n] = arr[x in taken ? taken[x] : x];
    taken[x] = --len in taken ? taken[len] : len;
  }
  return result;
};

export default BusinessList;
