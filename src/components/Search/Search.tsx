import React from 'react';

import styles from './Search.module.css';

interface SearchProps {
  onChange: (value: string) => void;
  value: string;
}

const Search: React.StatelessComponent<SearchProps> = ({ onChange, value = '' }) => {
  const ref = React.createRef<HTMLInputElement>();

  const handleEnter = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      handleClick();
    }
  };

  const handleClick = () => {
    if (ref.current) {
      onChange(ref.current.value);
    }
  };

  return (
    <div className={styles.root}>
      <input
        id={'search'}
        onKeyPress={handleEnter}
        placeholder={'My zip code is...'}
        ref={ref}
        type={'search'}
        defaultValue={value}
      />
      <button type={'button'} onClick={handleClick}>
        Let's Eat!
      </button>
    </div>
  );
};

export default Search;
