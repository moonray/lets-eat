# Let's Eat

Written by [Bala Bosch](http://github.com/moonray).

## Getting Started

Run `yarn start` and open the app in your browser at `http://localhost:3000/`.

## Notes

Due to a need for a Google Maps API key, a message will display on the maps stating `This page can't load Google Maps correctly.`

This can be safely ignored.
